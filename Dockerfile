FROM node:16-alpine AS development
ENV NODE_ENV development

WORKDIR /app

COPY ./app/package.json .
COPY ./app/package-lock.json .

RUN npm install --silent

COPY ./app .

EXPOSE 3000

CMD [ "npm", "start" ]

